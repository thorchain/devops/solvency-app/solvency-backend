package app

import (
	"net/http"

	"gitlab.com/thorchain/devops/solvency-app/solvency-backend/pkg/api"
	t "gitlab.com/thorchain/devops/solvency-app/solvency-backend/types"
)

func fetchLatestSnapshotAssetData(svc Service) http.HandlerFunc {
	return (func(w http.ResponseWriter, r *http.Request) {
		defer api.HandleError(w)

		symbols, err := svc.FetchAllSupportedAssetSymbols()
		api.CheckError(http.StatusInternalServerError, err)

		ch := make(chan *FetchAssetDatumResp, len(symbols))
		for _, sym := range symbols {
			go (func(sym string, ch chan *FetchAssetDatumResp) {
				data, err := svc.FetchLatestAssetDatumBySymbol(sym)

				var errStr string
				if err != nil {
					errStr = err.Error()
				}

				ch <- &FetchAssetDatumResp{
					AssetDatum: data,
					Error:      errStr,
					Symbol:     sym,
				}
			})(sym, ch)
		}

		resp := make([]*FetchAssetDatumResp, len(symbols))
		for i, _ := range resp {
			resp[i] = <-ch
		}

		api.WriteJSON(w, http.StatusNoContent, resp)
	})
}

type FetchAssetDatumResp struct {
	AssetDatum *t.AssetDatum `json:"assetDatum"`
	Error      string        `json:"error"`
	Symbol     string        `json:"symbol"`
}
