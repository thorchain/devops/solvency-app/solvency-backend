package app

import (
	t "gitlab.com/thorchain/devops/solvency-app/solvency-backend/types"
)

type Service interface {
	FetchLatestAssetDatumBySymbol(sym string) (*t.AssetDatum, error)
	FetchAllSupportedAssetSymbols() ([]string, error)
}
