package task

import (
	t "gitlab.com/thorchain/devops/solvency-app/solvency-backend/types"
)

type Service interface{}

type ThorChainDataFetcher interface {
	FetchAssetData() ([]t.AssetDatum, error)
	FetchSupportedAssets() ([]t.SupportedAsset, error)
}

type SolvencyDataFetcher struct {
	s  Service
	df ThorChainDataFetcher
}

func InitSolvencyDataFetcher(s Service, df ThorChainDataFetcher) SolvencyDataFetcher {
	return SolvencyDataFetcher{s, df}
}

func (s *SolvencyDataFetcher) Run() {

}

func (s *SolvencyDataFetcher) Kill() {}
