package types

import (
	"time"

	m "gitlab.com/thorchain/devops/solvency-app/solvency-backend/pkg/mongo"
)

// AssetDatum represents snapshotted solvency
// data for a particular asset.
type AssetDatum struct {
	CreatedAt    time.Time `bson:"createdAt" json:"createdAt"`
	SnapShotDate time.Time `bson:"snapShotDate" json:"snapshotDate"`

	PoolBalance   *m.Decimal `bson:"pb" json:"poolBalance"`
	VaultBalance  *m.Decimal `bson:"vb" json:"vaultBalance"`
	WalletBalance *m.Decimal `bson:"wb" json:"walletBalance"`

	Base64EncodedSymbol string `bson:"_id" json:"-"`    // don't return to the client, since it can be decoded on the server
	Symbol              string `bson:"-" json:"symbol"` // don't save since it is b64 encoded and set to id
	SearchTerms         string `bson:"search" json:"-"` // used for regex searching

	Status bool `bson:"s" json:"status"`
}

func (a *AssetDatum) GetID() interface{} {
	return a.Base64EncodedSymbol
}

// SupportedAsset acts as a look-up table item.
type SupportedAsset struct {
	Base64EncodedSymbol string `bson:"_id" json:"-"`    // ditto above
	Symbol              string `bson:"-" json:"symbol"` // ditto
}

func (s *SupportedAsset) GetID() interface{} {
	return s.Base64EncodedSymbol
}
