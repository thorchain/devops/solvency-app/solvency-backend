package mongo

import (
	"github.com/globalsign/mgo"
)

type IDGetter interface {
	GetID() interface{}
}

func IsDup(err error) bool {
	return mgo.IsDup(err)
}

func Insert(c *mgo.Collection, result interface{}, data IDGetter) error {
	var err error
	if result == nil {
		return c.Insert(data)
	} else {
		change := mgo.Change{
			Update:    data,
			ReturnNew: true,
			Upsert:    true,
		}
		_, err = c.Find(M{"_id": data.GetID()}).Apply(change, result)
	}
	return err
}

func Update(c *mgo.Collection, result, query, update interface{}) error {
	var err error
	if result == nil {
		err = c.Update(query, update)
	} else {
		change := mgo.Change{
			Update:    update,
			ReturnNew: true,
		}
		_, err = c.Find(query).Apply(change, result)
	}
	return err
}

func Upsert(c *mgo.Collection, result, query, update interface{}) error {
	var err error
	if result == nil {
		_, err = c.Upsert(query, update)
	} else {
		change := mgo.Change{
			Update:    update,
			Upsert:    true,
			ReturnNew: true,
		}
		_, err = c.Find(query).Apply(change, result)
		if p, ok := result.(PostProcessable); ok {
			p.PostProcess()
		}
	}
	return err
}

func UpdateAll(c *mgo.Collection, query, update interface{}) error {
	_, err := c.UpdateAll(query, update)
	return err
}

// First optional arg is Fields
// Second optional arg is slice of sort strings, ie. []string{"price", "-created_at"}
func Find(c *mgo.Collection, result, query interface{}, args ...interface{}) error {
	q := c.Find(query)
	if args != nil {
		if len(args) > 0 && args[0] != nil {
			q = q.Select(args[0])
		}
		if len(args) > 1 && args[1] != nil {
			q = q.Sort(args[1].([]string)...)
		}
	}
	if err := q.All(result); err != nil {
		return err
	}
	return nil
}

func FindOne(c *mgo.Collection, result, query interface{}, args ...interface{}) error {
	q := c.Find(query)
	if args != nil {
		if len(args) > 0 && args[0] != nil {
			q = q.Select(args[0])
		}
		if len(args) > 1 && args[1] != nil {
			q = q.Sort(args[1].([]string)...)
		}
	}
	if err := q.One(result); err != nil {
		return err
	}
	if p, ok := result.(PostProcessable); ok {
		p.PostProcess()
	}
	return nil
}

func Aggregate(c *mgo.Collection, result, pipe interface{}) error {
	if err := c.Pipe(pipe).All(result); err != nil {
		return err
	}
	return nil
}

func AggregateOne(c *mgo.Collection, result, pipe interface{}) error {
	if err := c.Pipe(pipe).One(result); err != nil {
		return err
	}
	return nil
}

func Remove(c *mgo.Collection, query interface{}) error {
	if _, err := c.RemoveAll(query); err != nil {
		return err
	}
	return nil
}

func CreateIndexKey(c *mgo.Collection, key ...string) error {
	return c.EnsureIndexKey(key...)
}

type Index = mgo.Index

func CreateIndex(c *mgo.Collection, index Index) error {
	return c.EnsureIndex(index)
}

func DeleteIndexes(c *mgo.Collection, keys ...string) error {
	for _, k := range keys {
		if err := c.DropIndex(k); err != nil {
			return err
		}
	}

	return nil
}
