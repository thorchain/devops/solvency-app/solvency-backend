package impl

import (
	"context"
	b64 "encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"math/big"
	"net/http"
	"net/url"
	"strings"
	"time"

	btcchaincfg "github.com/btcsuite/btcd/chaincfg"
	btcrpc "github.com/btcsuite/btcd/rpcclient"
	"github.com/btcsuite/btcutil"
	tcosm "github.com/cosmos/cosmos-sdk/types"
	ceth "github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/ethclient"
	bchchaincfg "github.com/gcash/bchd/chaincfg"
	bchrpc "github.com/gcash/bchd/rpcclient"
	"github.com/gcash/bchutil"
	ltcchaincfg "github.com/ltcsuite/ltcd/chaincfg"
	ltcrpc "github.com/ltcsuite/ltcd/rpcclient"
	"github.com/ltcsuite/ltcutil"
	cnance "gitlab.com/thorchain/binance-sdk/common/types"
	tnance "gitlab.com/thorchain/binance-sdk/types"
	cthor "gitlab.com/thorchain/thornode/common"
	tthor "gitlab.com/thorchain/thornode/x/thorchain/types"

	t "gitlab.com/thorchain/devops/solvency-app/solvency-backend/types"
)

type BlockchainClient struct {
	btcCfg         RPCConfig
	bchCfg         RPCConfig
	ltcCfg         RPCConfig
	binanceRPCHost string
	ethRPCHost     string
	thorIP         string
	thorPort       string
}
type RPCConfig struct {
	Host            string
	User            string
	Pwd             string
	Net             string
	DisableTLS      bool
	UseHTTPPostMode bool
}

func InitBlockchainClient(btcCfg, bchCfg, ltcCfg RPCConfig, binanceRPCHost, ethRPCHost, thorIP, thorPort string) BlockchainClient {
	return BlockchainClient{
		btcCfg:         btcCfg,
		bchCfg:         bchCfg,
		ltcCfg:         ltcCfg,
		binanceRPCHost: binanceRPCHost,
		ethRPCHost:     ethRPCHost,
		thorIP:         thorIP,
		thorPort:       thorPort,
	}
}

func (c *BlockchainClient) FetchAssetData() ([]t.AssetDatum, error) {
	pools, err := c.fetchPools()
	if err != nil {
		return nil, err
	}

	vaults, err := c.fetchAllVaults()
	if err != nil {
		return nil, err
	}

	totalBTC, err := c.fetchTotalBTCAmount(vaults)
	if err != nil {
		return nil, err
	}

	wallets, err := c.fetchWallets(vaults)
	if err != nil {
		return nil, err
	}

	txsOut, err := c.fetchTxsOut()
	if err != nil {
		return nil, err
	}

	return c.fetchAssetDataHelper(pools, vaults, txsOut, wallets, totalBTC)
}

func (c *BlockchainClient) fetchAssetDataHelper(
	pools tthor.Pools,
	vaults tthor.Vaults,
	txsOut cthor.Coins,
	wallets []cthor.Account,
	totalBTC float64,
) ([]t.AssetDatum, error) {

	return nil, nil
}

func (c *BlockchainClient) fetchTotalBTCAmount(vaults tthor.Vaults) (float64, error) {
	var total float64
	for _, item := range vaults {
		addr, err := item.PubKey.GetAddress(cthor.BTCChain)
		if err != nil {
			return 0, fmt.Errorf("fail to get BTC address for pub key(%s): %w", item.PubKey, err)
		}
		bal, err := c.getBTCAddressBalance(addr.String())
		if err != nil {
			fmt.Println("fail to get balance for address: ", addr.String(), err)
			continue
		}
		total += bal
	}
	return total, nil
}

func (c *BlockchainClient) fetchAllVaults() (tthor.Vaults, error) {
	v1, err := c.fetchAsgardVaults()
	if err != nil {
		return nil, err
	}

	v2, err := c.fetchYggdrasilVaults()
	if err != nil {
		return nil, err
	}

	return append(v1, v2...), nil
}

func (c *BlockchainClient) fetchTxsOut() (cthor.Coins, error) {
	buf, err := c.doGetFromThorNode("queue/outbound", time.Second*3)
	if err != nil {
		return nil, err
	}

	var txsOut []tthor.TxOutItem
	if err := json.Unmarshal(buf, &txsOut); err != nil {
		return nil, fmt.Errorf("fail to unmarshal txsOut: %w", err)
	}

	coins := cthor.Coins{}
	for _, tx := range txsOut {
		if !tx.OutHash.IsEmpty() {
			continue
		}
		if tx.InHash.Equals(cthor.BlankTxID) {
			continue
		}
		if !coins.Contains(tx.Coin) {
			coins = append(coins, tx.Coin)
			continue
		}
		for _, c := range coins {
			if c.Asset.Equals(tx.Coin.Asset) {
				c.Amount = c.Amount.Add(tx.Coin.Amount)
				break
			}
		}
	}

	return coins, nil
}

func (c *BlockchainClient) FetchSupportedAssets() ([]t.SupportedAsset, error) {
	pools, err := c.fetchPools()
	if err != nil {
		return nil, err
	}

	supportedAssets := make([]t.SupportedAsset, len(pools))
	for i, _ := range pools {
		supportedAssets[i] = t.SupportedAsset{Symbol: pools[i].Asset.String()}
	}

	return supportedAssets, nil
}

func (c *BlockchainClient) fetchPools() (tthor.Pools, error) {
	buf, err := c.doGetFromThorNode("pools", time.Second*3)
	if err != nil {
		return nil, err
	}

	var pools tthor.Pools
	if err := json.Unmarshal(buf, &pools); err != nil {
		return nil, fmt.Errorf("failed to unmarshal pools: %w", err)
	}

	return pools, err
}

func (c *BlockchainClient) fetchAsgardVaults() (tthor.Vaults, error) {
	buf, err := c.doGetFromThorNode("vaults/asgard", time.Second*3)
	if err != nil {
		return nil, fmt.Errorf("failed to get asgard vault: %w", err)
	}

	var resp []tthor.QueryVaultResp
	if err := json.Unmarshal(buf, &resp); err != nil {
		return nil, fmt.Errorf("failed to unmarshal vaults: %w", err)
	}

	var vaults tthor.Vaults
	for _, item := range resp {
		vaults = append(vaults, tthor.Vault{
			BlockHeight:           item.BlockHeight,
			PubKey:                item.PubKey,
			Coins:                 item.Coins,
			Type:                  item.Type,
			Status:                item.Status,
			StatusSince:           item.StatusSince,
			Membership:            item.Membership,
			Chains:                item.Chains,
			InboundTxCount:        item.InboundTxCount,
			OutboundTxCount:       item.OutboundTxCount,
			PendingTxBlockHeights: item.PendingTxBlockHeights,
			Routers:               item.Routers,
		})
	}
	return vaults, nil
}

func (c *BlockchainClient) fetchYggdrasilVaults() (tthor.Vaults, error) {
	buf, err := c.doGetFromThorNode("vaults/yggdrasil", time.Second*3)
	if err != nil {
		return nil, fmt.Errorf("failed to get yggdrasil vaults: %w", err)
	}

	var resp []tthor.QueryYggdrasilVaults
	if err := json.Unmarshal(buf, &resp); err != nil {
		return nil, fmt.Errorf("failed to unmarshal yggdrasil vaults")
	}

	var vaults tthor.Vaults
	for _, item := range resp {
		vaults = append(vaults, tthor.Vault{
			BlockHeight:           item.BlockHeight,
			PubKey:                item.PubKey,
			Coins:                 item.Coins,
			Type:                  item.Type,
			Status:                tthor.VaultStatus_ActiveVault,
			StatusSince:           item.StatusSince,
			Membership:            item.Membership,
			Chains:                item.Chains,
			InboundTxCount:        item.InboundTxCount,
			OutboundTxCount:       item.OutboundTxCount,
			PendingTxBlockHeights: item.PendingTxBlockHeights,
			Routers:               item.Routers,
		})
	}

	return vaults, nil
}

func (c *BlockchainClient) doGetFromThorNode(path string, timeout time.Duration) ([]byte, error) {
	url := fmt.Sprintf("http://%s:%s/thorchain/%s", c.thorIP, c.thorPort, path)
	client := http.Client{
		Timeout: timeout,
	}
	resp, err := client.Get(url)
	if err != nil {
		return nil, fmt.Errorf("failed to call %s :%w", url, err)
	}

	defer closeRespBody(resp.Body)

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("not expected status : %s", resp.Status)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read response body: %w", err)
	}

	return body, nil
}

func closeRespBody(body io.ReadCloser) {
	if err := body.Close(); err != nil {
		fmt.Println("fail to close response body", err)
	}
}

func (c *BlockchainClient) fetchWallets(vaults tthor.Vaults) ([]cthor.Account, error) {
	var accounts []cthor.Account
	for _, v := range vaults {
		addr, err := v.PubKey.GetAddress(cthor.BNBChain)
		if err != nil {
			return nil, fmt.Errorf("failed to get BNB address: %w", err)
		}
		acct, err := c.getAccountByAddress(addr.String())
		if err != nil {
			return nil, fmt.Errorf("failed get account detail for %s(%w)", addr, err)
		}

		btcAddr, err := v.PubKey.GetAddress(cthor.BTCChain)
		if err != nil {
			return nil, fmt.Errorf("failed to get BTC address: %w", err)
		}
		btcBalance, err := c.getBTCAddressBalance(btcAddr.String())
		if err != nil {
			return nil, fmt.Errorf("failed to get BTC balance: %w", err)
		}
		acct.Coins = append(acct.Coins, cthor.NewCoin(cthor.BTCAsset, tcosm.NewUint(uint64(btcBalance*float64(cthor.One)))))

		bchAddr, err := v.PubKey.GetAddress(cthor.BCHChain)
		if err != nil {
			return nil, fmt.Errorf("failed to get BCH address: %w", err)
		}
		bchBalance, err := c.getBCHAddressBalance(bchAddr.String())
		if err != nil {
			return nil, fmt.Errorf("failed to get BCH balance: %w", err)
		}
		acct.Coins = append(acct.Coins, cthor.NewCoin(cthor.BCHAsset, tcosm.NewUint(uint64(bchBalance*float64(cthor.One)))))

		ltcAddr, err := v.PubKey.GetAddress(cthor.LTCChain)
		if err != nil {
			return nil, fmt.Errorf("failed to get LTC address: %w", err)
		}
		ltcBalance, err := c.getLTCAddressBalance(ltcAddr.String())
		if err != nil {
			return nil, fmt.Errorf("failed to get LTC balance: %w", err)
		}
		acct.Coins = append(acct.Coins, cthor.NewCoin(cthor.LTCAsset, tcosm.NewUint(uint64(ltcBalance*float64(cthor.One)))))

		ethAddr, err := v.PubKey.GetAddress(cthor.ETHChain)
		if err != nil {
			return nil, fmt.Errorf("failed to get ETH address: %w", err)
		}

		chainContract := v.GetContract(cthor.ETHChain)
		if chainContract.IsEmpty() {
			continue
		}

		for _, coin := range v.Coins {
			if !coin.Asset.Chain.Equals(cthor.ETHChain) {
				continue
			}
			if coin.Asset.Equals(cthor.ETHAsset) {
				ethBalance, err := c.getETHAddressBalance(ethAddr.String(), chainContract.Router.String(), ETHToken)
				if err != nil {
					return nil, fmt.Errorf("failed to get ETH balance: %w", err)
				}
				acct.Coins = append(acct.Coins, cthor.NewCoin(cthor.ETHAsset, tcosm.NewUint(ethBalance)))

				continue
			}
			parts := strings.Split(coin.Asset.Symbol.String(), "-")
			ethBalance, err := c.getETHAddressBalance(ethAddr.String(), chainContract.Router.String(), parts[1])
			if err != nil {
				return nil, fmt.Errorf("failed to get ETH balance: %w", err)
			}
			acct.Coins = append(acct.Coins, cthor.NewCoin(coin.Asset, tcosm.NewUint(ethBalance)))
		}
		accounts = append(accounts, acct)
	}

	return accounts, nil
}

func (c *BlockchainClient) getAccountByAddress(accountAddr string) (cthor.Account, error) {
	fmt.Println("get binance account balance:", accountAddr)
	u, err := url.Parse(c.binanceRPCHost)
	if err != nil {
		return cthor.Account{}, err
	}

	u.Path = "/abci_query"
	v := u.Query()
	v.Set("path", fmt.Sprintf("\"/account/%s\"", accountAddr))
	u.RawQuery = v.Encode()
	resp, err := http.Get(u.String())
	if err != nil {
		return cthor.Account{}, err
	}
	defer closeRespBody(resp.Body)

	type queryResult struct {
		Jsonrpc string `json:"jsonrpc"`
		ID      string `json:"id"`
		Result  struct {
			Response struct {
				Key         string `json:"key"`
				Value       string `json:"value"`
				BlockHeight string `json:"height"`
			} `json:"response"`
		} `json:"result"`
	}

	var result queryResult
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return cthor.Account{}, err
	}
	err = json.Unmarshal(body, &result)
	if err != nil {
		return cthor.Account{}, err
	}

	data, err := b64.StdEncoding.DecodeString(result.Result.Response.Value)
	if err != nil {
		return cthor.Account{}, err
	}

	cdc := tnance.NewCodec()
	var acc cnance.AppAccount
	err = cdc.UnmarshalBinaryBare(data, &acc)
	if err != nil {
		return cthor.Account{}, err
	}
	coins, err := cthor.GetCoins(cthor.BNBChain, acc.BaseAccount.Coins)
	if err != nil {
		return cthor.Account{}, err
	}
	account := cthor.NewAccount(acc.BaseAccount.Sequence, acc.BaseAccount.AccountNumber, coins, acc.Flags > 0)

	return account, nil
}

func (c *BlockchainClient) getBTCAddressBalance(address string) (float64, error) {
	addr, err := btcutil.DecodeAddress(address, c.getBitcoinNetworkConfig())
	if err != nil {
		panic(err)
	}

	client := c.getBitcoinClient()
	result, err := client.ListUnspentMinMaxAddresses(0, 99999, []btcutil.Address{
		addr,
	})
	if err != nil {
		return 0.0, fmt.Errorf("failed to list accounts: %w", err)
	}

	var total float64
	for _, item := range result {
		total += item.Amount
	}

	return total, nil
}

func (c *BlockchainClient) getLitecoinClient() *ltcrpc.Client {
	client, err := ltcrpc.New(&ltcrpc.ConnConfig{
		Host:         c.ltcCfg.Host,
		User:         c.ltcCfg.User,
		Pass:         c.ltcCfg.Pwd,
		DisableTLS:   c.ltcCfg.DisableTLS,
		HTTPPostMode: c.ltcCfg.UseHTTPPostMode,
	}, nil)
	if err != nil {
		panic(err)
	}
	return client
}

func (c *BlockchainClient) getLitecoinNetworkConfig() *ltcchaincfg.Params {
	switch c.ltcCfg.Net {
	case ltcchaincfg.RegressionNetParams.Name:
		return &ltcchaincfg.RegressionNetParams
	case ltcchaincfg.MainNetParams.Name:
		return &ltcchaincfg.MainNetParams
	case ltcchaincfg.TestNet4Params.Name:
		return &ltcchaincfg.TestNet4Params
	}
	return nil
}

func (c *BlockchainClient) getBitcoinNetworkConfig() *btcchaincfg.Params {
	switch c.btcCfg.Net {
	case btcchaincfg.RegressionNetParams.Name:
		return &btcchaincfg.RegressionNetParams
	case btcchaincfg.MainNetParams.Name:
		return &btcchaincfg.MainNetParams
	case btcchaincfg.TestNet3Params.Name:
		return &btcchaincfg.TestNet3Params
	}
	return nil
}

func (c *BlockchainClient) getBitcoinClient() *btcrpc.Client {
	client, err := btcrpc.New(&btcrpc.ConnConfig{
		Host:         c.btcCfg.Host,
		User:         c.btcCfg.User,
		Pass:         c.btcCfg.Pwd,
		DisableTLS:   c.btcCfg.DisableTLS,
		HTTPPostMode: c.btcCfg.UseHTTPPostMode,
	}, nil)
	if err != nil {
		panic(err)
	}
	return client
}

func (c *BlockchainClient) getBCHAddressBalance(address string) (float64, error) {
	addr, err := bchutil.DecodeAddress(address, c.getBitcoinCashNetworkConfig())
	if err != nil {
		panic(err)
	}

	client := c.getBitcoinCashClient()
	result, err := client.ListUnspentMinMaxAddresses(0, 99999, []bchutil.Address{
		addr,
	})
	if err != nil {
		return 0.0, fmt.Errorf("failed to list accounts: %w", err)
	}

	var total float64
	for _, item := range result {
		total += item.Amount
	}
	return total, nil
}

func (c *BlockchainClient) getBitcoinCashNetworkConfig() *bchchaincfg.Params {
	switch c.bchCfg.Net {
	case bchchaincfg.RegressionNetParams.Name:
		return &bchchaincfg.RegressionNetParams
	case bchchaincfg.MainNetParams.Name:
		return &bchchaincfg.MainNetParams
	case bchchaincfg.TestNet3Params.Name:
		return &bchchaincfg.TestNet3Params
	}
	return nil
}

func (c *BlockchainClient) getBitcoinCashClient() *bchrpc.Client {
	client, err := bchrpc.New(&bchrpc.ConnConfig{
		Host:         c.bchCfg.Host,
		User:         c.bchCfg.User,
		Pass:         c.bchCfg.Pwd,
		DisableTLS:   c.bchCfg.DisableTLS,
		HTTPPostMode: c.bchCfg.UseHTTPPostMode,
	}, nil)
	if err != nil {
		panic(err)
	}
	return client
}

func (c *BlockchainClient) getLTCAddressBalance(address string) (float64, error) {
	addr, err := ltcutil.DecodeAddress(address, c.getLitecoinNetworkConfig())
	if err != nil {
		panic(err)
	}

	client := c.getLitecoinClient()
	result, err := client.ListUnspentMinMaxAddresses(0, 99999, []ltcutil.Address{
		addr,
	})
	if err != nil {
		return 0.0, fmt.Errorf("fail to list account: %w", err)
	}
	var total float64
	for _, item := range result {
		total += item.Amount
	}
	return total, nil
}

const ETHToken = `0x0000000000000000000000000000000000000000`

func (c *BlockchainClient) getETHAddressBalance(address, contract, token string) (uint64, error) {
	client, err := c.getETHClient()
	if err != nil {
		return 0, fmt.Errorf("failed to get ETH client: %w", err)
	}

	if token == ETHToken {
		resp, err := client.BalanceAt(context.TODO(), ceth.HexToAddress(address), nil)
		if err != nil {
			return 0.0, fmt.Errorf("failed to get the balance of address: %s, %w", address, err)
		}
		// convert ETH balance from 1e18 -> 1e8
		return big.NewInt(0).Div(resp, big.NewInt(1e10)).Uint64(), nil
	}
	router, err := NewRouter(ceth.HexToAddress(contract), client)
	if err != nil {
		return 0, fmt.Errorf("failed to create router object: %w", err)
	}

	v, err := router.VaultAllowance(nil, ceth.HexToAddress(address), ceth.HexToAddress(token))
	if err != nil {
		return 0, fmt.Errorf("failed to get vault allowance: %w", err)
	}
	// convert ETH balance from 1e18 -> 1e8
	if strings.EqualFold(token, "0X62E273709DA575835C7F6AEF4A31140CA5B1D190") {
		return big.NewInt(0).Mul(v, big.NewInt(100)).Uint64(), nil
	}
	if strings.EqualFold(token, "0X242AD49DACD38AC23CAF2CCC118482714206BED4") {
		return v.Uint64(), nil
	}
	return big.NewInt(0).Div(v, big.NewInt(1e10)).Uint64(), nil
}

func (c *BlockchainClient) getRUNEBalance(moduleName string) (tcosm.Uint, error) {
	buf, err := c.doGetFromThorNode(fmt.Sprintf("balance/module/%s", moduleName), time.Second*3)
	if err != nil {
		return tcosm.ZeroUint(), fmt.Errorf("failed to get %s balance from THORChain: %w", moduleName, err)
	}

	var resp struct {
		Name    string           `json:"name"`
		Address tcosm.AccAddress `json:"address"`
		Coins   tcosm.Coins      `json:"coins"`
	}
	if err := json.Unmarshal(buf, &resp); err != nil {
		return tcosm.ZeroUint(), fmt.Errorf("fail to unmarshal result: %w", err)
	}

	for _, item := range resp.Coins {
		if strings.EqualFold(item.Denom, "rune") {
			return tcosm.NewUintFromBigInt(item.Amount.BigInt()), nil
		}
	}

	return tcosm.ZeroUint(), fmt.Errorf("didn't find RUNE")
}

func (c *BlockchainClient) getETHClient() (*ethclient.Client, error) {
	return ethclient.Dial(c.ethRPCHost)
}
