package impl

import (
	t "gitlab.com/thorchain/devops/solvency-app/solvency-backend/types"
)

type Service struct {
	Repo Repository
}

func (s *Service) FetchLatestAssetDatumBySymbol(sym string) (*t.AssetDatum, error) {
	return s.Repo.FindLatestAssetDatumBySymbol(sym)
}

func (s *Service) FetchAllSupportedAssetSymbols() ([]string, error) {
	return s.Repo.FindAllSupportedAssetSymbols()
}

type Repository interface {
	FindAllSupportedAssetSymbols() ([]string, error)
	FindLatestAssetDatumBySymbol(sym string) (*t.AssetDatum, error)
}
