package scheduler

import (
	"time"
)

type Task interface {
	Kill()
	Run()
}

type Scheduler struct {
	ticker *time.Ticker
	task   Task
}

func Init(ticker *time.Ticker, task Task) Scheduler {
	return Scheduler{ticker, task}
}

func (s *Scheduler) Start() {
	for ; true; <-s.ticker.C {
		s.task.Run()
	}
}

func (s *Scheduler) Close() {
	s.ticker.Stop()
	s.task.Kill()
}
