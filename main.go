package main

import (
	"flag"
	"log"
	"os"
	"strconv"
	"time"

	"github.com/go-chi/cors"
	"github.com/joho/godotenv"

	"gitlab.com/thorchain/devops/solvency-app/solvency-backend/impl"
	"gitlab.com/thorchain/devops/solvency-app/solvency-backend/pkg/api"
	m "gitlab.com/thorchain/devops/solvency-app/solvency-backend/pkg/mongo"
	sched "gitlab.com/thorchain/devops/solvency-app/solvency-backend/scheduler"
	"gitlab.com/thorchain/devops/solvency-app/solvency-backend/task"
	"gitlab.com/thorchain/devops/solvency-app/solvency-backend/v1/app"
)

// wire up the app server and snapshot scheduler
func main() {
	envPath := flag.String("env", ".env", "path for environment variables")
	flag.Parse()
	if err := godotenv.Load(*envPath); err != nil {
		log.Fatalln("Error loading environment variables: ", err)
	}

	mongoHost := os.Getenv("MONGO_HOST")
	mongoUser := os.Getenv("MONGO_USER")
	mongoPwd := os.Getenv("MONGO_PWD")
	mongoDBName := os.Getenv("MONGO_DB_NAME")

	store, err := m.InitStore(mongoDBName, mongoHost, mongoPwd, mongoUser)
	checkError(err)

	appServer := initServer(store)
	sc := initFetchSolvencyDataScheduler(store)
	go sc.Start()
	defer cleanup(&appServer, &sc)

	apiServer := api.InitServer()
	apiServer.Router.Use(cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"GET", "PATCH", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: true,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	}).Handler)
	apiServer.Router.Mount("/api/v1/solvency", appServer.Router())

	apiServer.Start(os.Getenv("PORT"))
}

func initServer(store m.Store) app.Server {
	return app.Init(&impl.Service{Repo: &impl.DB{Store: &store}})
}

func initFetchSolvencyDataScheduler(store m.Store) sched.Scheduler {
	var (
		btcHost        = os.Getenv("BTC_HOST")
		btcUser        = os.Getenv("BTC_USER")
		btcPwd         = os.Getenv("BTC_PWD")
		btcNet         = os.Getenv("BTC_NET")
		bchHost        = os.Getenv("BCH_HOST")
		bchUser        = os.Getenv("BCH_USER")
		bchPwd         = os.Getenv("BCH_PWD")
		bchNet         = os.Getenv("BCH_NET")
		ltcHost        = os.Getenv("LTC_HOST")
		ltcUser        = os.Getenv("LTC_USER")
		ltcPwd         = os.Getenv("LTC_PWD")
		ltcNet         = os.Getenv("LTC_NET")
		binanceRPCHost = os.Getenv("BINANCE_RPC_HOST")
		ethRPCHost     = os.Getenv("ETH_RPC_HOST")
		thorIP         = os.Getenv("THOR_IP")
		thorPort       = os.Getenv("THOR_PORT")
	)
	secs, err := strconv.Atoi(os.Getenv("SNAP_SHOT_INTERVAL_SECONDS"))
	checkError(err)
	btcDisablTLS, err := strconv.ParseBool(os.Getenv("BTC_DISABLE_TLS"))
	checkError(err)
	btcUseHTTPPostMode, err := strconv.ParseBool(os.Getenv("BTC_USE_HTTP_POST_MODE"))
	checkError(err)
	bchDisablTLS, err := strconv.ParseBool(os.Getenv("BCH_DISABLE_TLS"))
	checkError(err)
	bchUseHTTPPostMode, err := strconv.ParseBool(os.Getenv("BCH_USE_HTTP_POST_MODE"))
	checkError(err)
	ltcDisablTLS, err := strconv.ParseBool(os.Getenv("LTC_DISABLE_TLS"))
	checkError(err)
	ltcUseHTTPPostMode, err := strconv.ParseBool(os.Getenv("LTC_USE_HTTP_POST_MODE"))
	checkError(err)

	blockchainClient := impl.InitBlockchainClient(
		impl.RPCConfig{
			Host:            btcHost,
			User:            btcUser,
			Pwd:             btcPwd,
			Net:             btcNet,
			DisableTLS:      btcDisablTLS,
			UseHTTPPostMode: btcUseHTTPPostMode,
		},
		impl.RPCConfig{
			Host:            bchHost,
			User:            bchUser,
			Pwd:             bchPwd,
			Net:             bchNet,
			DisableTLS:      bchDisablTLS,
			UseHTTPPostMode: bchUseHTTPPostMode,
		},
		impl.RPCConfig{
			Host:            ltcHost,
			User:            ltcUser,
			Pwd:             ltcPwd,
			Net:             ltcNet,
			DisableTLS:      ltcDisablTLS,
			UseHTTPPostMode: ltcUseHTTPPostMode,
		},
		binanceRPCHost,
		ethRPCHost,
		thorIP,
		thorPort,
	)
	s := task.InitSolvencyDataFetcher(
		&impl.Service{Repo: &impl.DB{Store: &store}},
		&blockchainClient,
	)

	return sched.Init(
		time.NewTicker(time.Second*time.Duration(secs)),
		&s,
	)
}

func checkError(err error) {
	if err != nil {
		log.Fatalln("Error: ", err)
	}
}

type Closer interface {
	Close()
}

func cleanup(closers ...Closer) {
	for _, c := range closers {
		c.Close()
	}
}
