module gitlab.com/thorchain/devops/solvency-app/solvency-backend

go 1.16

replace github.com/binance-chain/tss-lib => gitlab.com/thorchain/tss/tss-lib v0.0.0-20201118045712-70b2cb4bf916

replace github.com/gogo/protobuf => github.com/regen-network/protobuf v1.3.2-alpha.regen.4

require (
	github.com/OneOfOne/xxhash v1.2.5 // indirect
	github.com/btcsuite/btcd v0.21.0-beta // indirect
	github.com/btcsuite/btcutil v1.0.2
	github.com/cosmos/cosmos-sdk v0.41.0
	github.com/ethereum/go-ethereum v1.9.25
	github.com/gcash/bchd v0.17.1 // indirect
	github.com/gcash/bchutil v0.0.0-20201025062739-fc759989ee3e
	github.com/globalsign/mgo v0.0.0-20181015135952-eeefdecb41b8
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/go-chi/cors v1.2.0
	github.com/go-chi/render v1.0.1
	github.com/gopherjs/gopherjs v0.0.0-20190430165422-3e4dfb77656c // indirect
	github.com/gorilla/schema v1.2.0
	github.com/joho/godotenv v1.3.0
	github.com/ltcsuite/ltcd v0.20.1-beta.0.20201210074626-c807bfe31ef0 // indirect
	github.com/ltcsuite/ltcutil v1.0.2-beta
	github.com/mitchellh/mapstructure v1.3.3 // indirect
	github.com/shopspring/decimal v1.2.0
	github.com/sirupsen/logrus v1.6.0
	gitlab.com/thorchain/binance-sdk v1.2.3-0.20210117202539-d569b6b9ba5d
	gitlab.com/thorchain/thornode v0.14.1-0.20210216050653-c0c300b85dd1
)
